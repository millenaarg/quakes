Zotonic example site
=====================

Zotonic site that displays some statistics on earthquakes in the Netherlands.  
Retrieves http://www.opengis.eu/geoservice/bevingen.json, normalizes the data and saves it to the DB.  
Displays some statistics.

This is not making use of Zotonic's excellent CMS functionality. It is an exploration  
of how to leverage Zotonic for more "custom" behaviour.


Functionality
--------------
 - Retrieve JSON
 - Normalize data and save to DB
 - Create custom DB
 	- Zotonic #column_def record and psql library slightly adapted to
	  allow for creation of row with "unique" constraint. Not present in this repo.
 - Some basic SQL injection protection
 - Some basic Bootstrap use
 - Some basic use of ErlyDTL templating

To do
----------
 - Clean up!
    - Functions can be cleaner
    - Templates can be cleaner
 - Model query functions ar a bit hacky
 - Postback for data refresh on pageload needs refinement
